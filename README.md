# Cloud Functions API

In this project we will create an API by using Firebase Cloud Functions. Our app will enable users to get movies info from OMDb and add it to our API in Firebase. There movie info can be edited or deleted when user is signed in Firebase.

Blog post -> https://amirjanus.gitlab.io/blog/2019/08/20/cloud-functions-api/

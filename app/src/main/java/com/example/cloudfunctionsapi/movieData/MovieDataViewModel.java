package com.example.cloudfunctionsapi.movieData;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

public class MovieDataViewModel extends ViewModel {
    
    protected MutableLiveData<List<MovieData>> m_readData;
    protected MutableLiveData<String> m_createdId;
    protected MutableLiveData<String> m_deletedId;
    protected MutableLiveData<String> m_updatedId;
    
    public MovieDataViewModel() {
        m_readData = new MutableLiveData<>();
        m_createdId = new MutableLiveData<>();
        m_deletedId = new MutableLiveData<>();
        m_updatedId = new MutableLiveData<>();
    }
    
    public List<MovieData> GetCachedData() {
        return m_readData.getValue();
    }
    
    public LiveData<List<MovieData>> GetReadData() {
        return m_readData;
    }
    
    public LiveData<String> GetCreatedId() {
        return m_createdId;
    }
    
    public LiveData<String> GetDeletedId() {
        return m_deletedId;
    }
    
    public LiveData<String> GetUpdatedId() {
        return m_updatedId;
    }
    
}

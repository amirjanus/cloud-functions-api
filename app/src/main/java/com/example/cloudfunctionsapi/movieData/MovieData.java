package com.example.cloudfunctionsapi.movieData;

import com.example.cloudfunctionsapi.firemovies.FmdbReqData;
import com.example.cloudfunctionsapi.firemovies.FmdbRes;
import com.example.cloudfunctionsapi.omdb.OmdbRes;

public class MovieData {
    
    public String id;
    public String title;
    public int year;
    public String runtime;
    public String genre;
    public String director;
    public String actors;
    public String country;
    public String plot;
    public boolean response;
    public String error;
    
    public MovieData() {}
    
    public MovieData( OmdbRes omdbRes ) {
        title = omdbRes.Title;
        year = omdbRes.Year;
        runtime = omdbRes.Runtime;
        genre = omdbRes.Genre;
        director = omdbRes.Director;
        actors = omdbRes.Actors;
        country = omdbRes.Country;
        plot = omdbRes.Plot;
        response = omdbRes.Response;
        error = omdbRes.Error;
    }
    
    public MovieData( FmdbRes fmdbRes ) {
        id = fmdbRes.id;
        title = fmdbRes.title;
        year = fmdbRes.year;
        runtime = fmdbRes.runtime;
        genre = fmdbRes.genre;
        director = fmdbRes.director;
        actors = fmdbRes.actors;
        country = fmdbRes.country;
        plot = fmdbRes.plot;
        response = fmdbRes.response;
        error = fmdbRes.error;
    }
    
    public FmdbReqData ToFmdbReqData() {
        FmdbReqData fmdbReqData = new FmdbReqData();
    
        fmdbReqData.title = title;
        fmdbReqData.year = year;
        fmdbReqData.runtime = runtime;
        fmdbReqData.genre = genre;
        fmdbReqData.director = director;
        fmdbReqData.actors = actors;
        fmdbReqData.country = country;
        fmdbReqData.plot = plot;
        
        return fmdbReqData;
    }

}

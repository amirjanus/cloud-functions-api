package com.example.cloudfunctionsapi.activities.addItem;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.example.cloudfunctionsapi.R;
import com.example.cloudfunctionsapi.common.Common;
import com.example.cloudfunctionsapi.common.FirebaseAuthIdToken;
import com.example.cloudfunctionsapi.common.ReusableShowMovie;
import com.example.cloudfunctionsapi.firemovies.FmdbClientApi;
import com.example.cloudfunctionsapi.firemovies.FmdbReqBody;
import com.example.cloudfunctionsapi.firemovies.FmdbToken;
import com.example.cloudfunctionsapi.movieData.MovieData;
import com.example.cloudfunctionsapi.omdb.OmdbClientApi;
import com.example.cloudfunctionsapi.retrofit.fmdb.RfFmdb;
import com.example.cloudfunctionsapi.retrofit.omdb.RfOmdb;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

public class AddItemActivity extends AppCompatActivity {
    
    private static final String KEY_HTTP_CALL_STARTED = "HTTP_CALL_STARTED";
    private static final String KEY_ITEM_UPLOADED = "ITEM_UPLOADED";
    
    private FmdbClientApi m_fmdbClientApi;
    private OmdbClientApi m_omdbClientApi;
    
    private EditText m_etTitle;
    private EditText m_etYear;
    private Button m_btnSearch;
    private TextView m_textNotFound;
    private ProgressBar m_progressBar;
    private View m_includeShowMovie;
    private FloatingActionButton m_fabAdd;
    private ReusableShowMovie m_showMovie;
    
    // Set to true when http call if made.
    private boolean m_isHttpCallStarted;
    
    // Set to true of movie was uploaded to Firestore.
    private boolean m_wasItemUploaded;
    
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_add_item );
        
        InitViews();
        InitViewModel();
        InitButtonListeners();
        
        RestoreInstanceState( savedInstanceState );
    }
    
    @Override
    protected void onSaveInstanceState( Bundle outState ) {
        outState.putBoolean( KEY_HTTP_CALL_STARTED, m_isHttpCallStarted );
        outState.putBoolean( KEY_ITEM_UPLOADED, m_wasItemUploaded );
        
        super.onSaveInstanceState( outState );
    }
    
    @Override
    public void onBackPressed() {
        if ( m_wasItemUploaded ) {
            setResult( RESULT_OK );
        } else {
            setResult( RESULT_CANCELED );
        }
        
        finish();
    }
    
    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        if ( item.getItemId() == android.R.id.home ) {
            if ( m_wasItemUploaded ) {
                setResult( RESULT_OK );
            } else {
                setResult( RESULT_CANCELED );
            }
            
            finish();
            
            return true;
        } else {
            return super.onOptionsItemSelected( item );
        }
    }
    
    /**
     * Initialize Views.
     */
    private void InitViews() {
        m_etTitle = findViewById( R.id.editText_title );
        m_etYear = findViewById( R.id.editText_year );
        m_btnSearch = findViewById( R.id.button_search );
        m_textNotFound = findViewById( R.id.text_notFound );
        m_progressBar = findViewById( R.id.progressBar );
        m_includeShowMovie = findViewById( R.id.include_showMovie );
        m_fabAdd = findViewById( R.id.floatingActionButton_add );
        
        m_showMovie = new ReusableShowMovie( m_includeShowMovie );
    }
    
    /**
     * Initialize http clients for Omdb and Fmdb apis.
     */
    private void InitViewModel() {
        m_omdbClientApi = ViewModelProviders.of( this ).get( OmdbClientApi.class );
    
        if ( !m_omdbClientApi.IsOmdbSet() ) {
            m_omdbClientApi.SetOmdb( new RfOmdb(), getString( R.string.apikey ) );
        }
        
        m_omdbClientApi.GetReadData().observe( this, data -> {
            m_showMovie.SetViews( data.get( 0 ) );
            
            OnSearchDone( data.get( 0 ) );
        } );
        
        m_fmdbClientApi = ViewModelProviders.of( this ).get( FmdbClientApi.class );
    
        if ( !m_fmdbClientApi.IsFmdbSet() ) {
            m_fmdbClientApi.SetFmdb( new RfFmdb() );
        }
        
        m_fmdbClientApi.GetCreatedId().observe( this, id -> {
            String message;
            
            if ( id.compareTo( "-1" ) == 0 ) {
                message = "Error uploading file.";
            } else {
                message = "File uploaded with id: " + id;
                
                m_wasItemUploaded = true;
            }
            
            OnAddDone();
            
            Common.MakeSnackbar( this, findViewById( R.id.parent ), message );
        } );
    }
    
    /**
     * Set Button click listeners.
     */
    private void InitButtonListeners() {
        m_btnSearch.setOnClickListener( view -> OnBtnSearchClick() );
        
        m_fabAdd.setOnClickListener( view -> OnButtonAddClick() );
    }
    
    /**
     * Checks if activity was in process of waiting for http response to return before configuration
     * changed.
     */
    private void RestoreInstanceState( Bundle savedInstanceState ) {
        if ( savedInstanceState != null ) {
            
            if ( savedInstanceState.getBoolean( KEY_HTTP_CALL_STARTED ) ) {
                OnHttpCallStarted();
            }
            
            m_wasItemUploaded = savedInstanceState.getBoolean( KEY_ITEM_UPLOADED );
            
        }
    }
    
    /**
     * Handles click on add button. Uploads movie data from Omdb to Firestore.
     */
    private void OnButtonAddClick() {
        FirebaseAuthIdToken.GetToken( idToken -> {
            if ( idToken != null ) {
                
                FmdbReqBody fmdbReqBody = new FmdbReqBody();
                fmdbReqBody.token = new FmdbToken( idToken );
                fmdbReqBody.data = m_omdbClientApi.GetCachedData().get( 0 ).ToFmdbReqData();
                
                m_fmdbClientApi.CreateMovie( fmdbReqBody );
                
                OnHttpCallStarted();
                
            } else {
                Common.MakeSnackbar( this, findViewById( R.id.parent ), "There was a problem with authentication" );
            }
        } );
    }
    
    /**
     * Shows hidden UI views after http response was received and item was added to Firestore.
     */
    private void OnAddDone() {
        m_isHttpCallStarted = false;
        
        m_btnSearch.setEnabled( true );
        m_progressBar.setVisibility( View.GONE );
        m_includeShowMovie.setVisibility( View.VISIBLE );
        m_fabAdd.show();
    }
    
    /**
     * Called when search button is clicked. Extracts data from EditText widgets and call Omdb api
     * to get requested movie.
     */
    private void OnBtnSearchClick() {
        String title = m_etTitle.getText().toString();
        
        if ( title.length() > 0 ) {
            
            int year;
            
            try {
                year = Integer.parseInt( m_etYear.getText().toString() );
            } catch ( NumberFormatException ignored ) {
                year = 0;
            }
            
            if ( year > 0 ) {
                m_omdbClientApi.GetMovieByTitleAndYear( title, year );
            } else {
                m_omdbClientApi.GetMovieByTitle( title );
            }
            
            OnHttpCallStarted();
            
            Common.HideKeyboard( this );
            
        } else {
            Common.MakeSnackbar( this, findViewById( R.id.parent ), "Enter movie title" );
        }
    }
    
    /**
     * Hides UI Views and disables Buttons so that user cannot initiate another http request until
     * current finishes.
     */
    private void OnHttpCallStarted() {
        m_isHttpCallStarted = true;
        
        m_btnSearch.setEnabled( false );
        m_textNotFound.setVisibility( View.GONE );
        m_progressBar.setVisibility( View.VISIBLE );
        m_includeShowMovie.setVisibility( View.GONE );
        m_fabAdd.hide();
    }
    
    /**
     * Shows hidden UI views after http response was received and list of movies was received.
     *
     * @param movieData Requested movie data.
     */
    private void OnSearchDone( MovieData movieData ) {
        m_isHttpCallStarted = false;
        
        m_btnSearch.setEnabled( true );
        
        if ( movieData.response ) {
            
            m_includeShowMovie.setVisibility( View.VISIBLE );
            m_fabAdd.show();
            
        } else {
            m_textNotFound.setVisibility( View.VISIBLE );
        }
        
        m_progressBar.setVisibility( View.GONE );
    }
    
}

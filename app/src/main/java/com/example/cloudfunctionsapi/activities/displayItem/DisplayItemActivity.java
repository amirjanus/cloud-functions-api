package com.example.cloudfunctionsapi.activities.displayItem;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.example.networking.R;
import com.example.networking.activities.client.ClientActivity;
import com.example.networking.common.Common;
import com.example.networking.common.ReusableShowMovie;
import com.example.networking.firemovies.FmdbClientApi;
import com.example.networking.movieData.MovieData;
import com.example.networking.retrofit.fmdb.RfFmdb;

import java.util.List;

public class DisplayItemActivity extends AppCompatActivity {
    
    private static final String KEY_HTTP_CALL_STARTED = "HTTP_CALL_STARTED";
    private static final String KEY_ID = "ID";
    
    private FmdbClientApi m_fmdbClientApi;
    
    private ReusableShowMovie m_reusable;
    private ProgressBar m_progressBar;
    private View m_include;
    private View m_parent;
    
    private String m_id;
    
    private boolean m_wasHttpCallStarted;
    
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_display_item );
        
        Intent intent = getIntent();
        m_id = intent.getStringExtra( ClientActivity.EXTRA_ID );
        
        InitViews();
        InitViewModel();
        
        RestoreInstanceState( savedInstanceState );
    }
    
    @Override
    protected void onSaveInstanceState( Bundle outState ) {
        outState.putBoolean( KEY_HTTP_CALL_STARTED, m_wasHttpCallStarted );
        outState.putString( KEY_ID, m_id );
        
        super.onSaveInstanceState( outState );
    }
    
    /**
     * Initialize Views.
     */
    private void InitViews() {
        m_progressBar = findViewById( R.id.progressBar );
        m_parent = findViewById( R.id.parent );
        m_include = findViewById( R.id.include );
        
        m_reusable = new ReusableShowMovie( m_include );
    }
    
    /**
     * Initialize http client from Fmdb.
     */
    private void InitViewModel() {
        m_fmdbClientApi = ViewModelProviders.of( this ).get( FmdbClientApi.class );
    
        if ( !m_fmdbClientApi.IsFmdbSet() ) {
            m_fmdbClientApi.SetFmdb( new RfFmdb() );
        }
        
        m_fmdbClientApi.GetReadData().observe( this, movieDataList -> OnGetMovieDone( movieDataList ) );
    }
    
    private void RestoreInstanceState( Bundle savedInstanceState ) {
        // Check if http call was started in previous instance.
        if ( savedInstanceState != null ) {
            
            m_wasHttpCallStarted = savedInstanceState.getBoolean( KEY_HTTP_CALL_STARTED );
            m_id = savedInstanceState.getString( KEY_ID );
            
            if ( m_wasHttpCallStarted ) {
                
                m_progressBar.setVisibility( View.VISIBLE );
                
                return;
                
            }
        }
        
        // Check if movie data was fetched in previous instance.
        List<MovieData> movieDataList = m_fmdbClientApi.GetCachedData();
        
        if ( movieDataList != null && movieDataList.size() > 0 ) {
            
            OnGetMovieDone( movieDataList );
            
            return;
            
        }
        
        // Get movie from Fmdb.
        m_fmdbClientApi.GetMovieById( m_id );
    }
    
    /**
     * Called when http call to get movie returns.
     *
     * @param movieDataList List of movie data.
     */
    private void OnGetMovieDone( List<MovieData> movieDataList ) {
        m_wasHttpCallStarted = false;
        
        m_progressBar.setVisibility( View.GONE );
        m_include.setVisibility( View.VISIBLE );
        
        if ( movieDataList != null ) {
            
            MovieData movieData = movieDataList.get( 0 );
            
            if ( movieData.response ) {
                m_reusable.SetViews( movieData );
            }
            
            return;
            
        }
        
        Common.MakeSnackbar( this, m_parent, "There was and error getting movie" );
    }
    
}

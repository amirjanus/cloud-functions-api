package com.example.cloudfunctionsapi.activities.client;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.networking.R;
import com.example.networking.movieData.MovieData;

import java.util.ArrayList;

public class MoviesClientAdapter extends RecyclerView.Adapter<MoviesClientAdapter.MovieViewHolder> {
    
    public interface OnItemClickListener {
        void OnItemClick( String id );
    }
    
    private OnItemClickListener m_listener;
    
    private ArrayList<MovieData> m_dataset;
    
    public MoviesClientAdapter( ArrayList<MovieData> dataset ) {
        m_dataset = dataset;
    }
    
    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder( @NonNull ViewGroup parent, int viewType ) {
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.item_movie_short, parent, false );
        
        return new MovieViewHolder( view );
    }
    
    @Override
    public void onBindViewHolder( @NonNull MovieViewHolder holder, int position ) {
        MovieData current = m_dataset.get( position );
        
        holder.m_title.setText( current.title );
        holder.m_directors.setText( current.director );
        holder.m_actors.setText( current.actors );
        holder.m_genre.setText( current.genre );
        holder.m_runtime.setText( current.runtime );
        holder.m_country.setText( current.country );
        
        holder.m_itemView.setOnClickListener( view -> {
            if ( m_listener != null ) {
                m_listener.OnItemClick( current.id );
            }
        } );
    }
    
    @Override
    public int getItemCount() {
        return m_dataset.size();
    }
    
    public void SetOnItemClickListener( OnItemClickListener listener ) {
        m_listener = listener;
    }
    
    public class MovieViewHolder extends RecyclerView.ViewHolder {
        
        private TextView m_title;
        private TextView m_directors;
        private TextView m_actors;
        private TextView m_genre;
        private TextView m_runtime;
        private TextView m_country;
        
        private View m_itemView;
        
        public MovieViewHolder( @NonNull View itemView ) {
            super( itemView );
            
            m_title = itemView.findViewById( R.id.text_title );
            m_directors = itemView.findViewById( R.id.text_directors );
            m_actors = itemView.findViewById( R.id.text_actors );
            m_genre = itemView.findViewById( R.id.text_genre );
            m_runtime = itemView.findViewById( R.id.text_runtime );
            m_country = itemView.findViewById( R.id.text_country );
            
            m_itemView = itemView;
        }
    }
}

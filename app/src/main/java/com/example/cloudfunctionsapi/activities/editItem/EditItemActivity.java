package com.example.cloudfunctionsapi.activities.editItem;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import com.example.networking.R;
import com.example.networking.activities.admin.AdminActivity;
import com.example.networking.common.Common;
import com.example.networking.common.FirebaseAuthIdToken;
import com.example.networking.common.ReusableEditMovie;
import com.example.networking.firemovies.FmdbClientApi;
import com.example.networking.firemovies.FmdbReqBody;
import com.example.networking.firemovies.FmdbToken;
import com.example.networking.movieData.MovieData;
import com.example.networking.retrofit.fmdb.RfFmdb;

import java.util.List;

public class EditItemActivity extends AppCompatActivity {
    
    private static final String KEY_HTTP_CALL_STARTED = "HTTP_CALL_STARTED";
    private static final String KEY_EDITED = "EDITED";
    private static final String KEY_ID = "ID";
    
    private FmdbClientApi m_fmdbClientApi;
    
    private Button m_btnReset;
    private Button m_btnUpdate;
    private ProgressBar m_progressBar;
    private View m_includeEdit;
    private View m_parent;
    
    private ReusableEditMovie m_reusable;
    
    private String m_id;
    
    private boolean m_wasEdited;
    private boolean m_isHttpCallStarted;
    
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_edit_item );
        
        GetIdFromIntent();
        InitViews();
        InitViewModel();
        InitButtonListeners();
        
        RestoreInstanceState( savedInstanceState );
    }
    
    @Override
    protected void onSaveInstanceState( Bundle outState ) {
        outState.putBoolean( KEY_HTTP_CALL_STARTED, m_isHttpCallStarted );
        outState.putBoolean( KEY_EDITED, m_wasEdited );
        outState.putString( KEY_ID, m_id );
        
        super.onSaveInstanceState( outState );
    }
    
    @Override
    public void onBackPressed() {
        if ( m_wasEdited ) {
            setResult( RESULT_OK );
        } else {
            setResult( RESULT_CANCELED );
        }
        
        finish();
    }
    
    @Override
    public boolean onOptionsItemSelected( MenuItem item ) {
        if ( item.getItemId() == android.R.id.home ) {
            if ( m_wasEdited ) {
                setResult( RESULT_OK );
            } else {
                setResult( RESULT_CANCELED );
            }
            
            finish();
            
            return true;
        } else {
            return super.onOptionsItemSelected( item );
        }
    }
    
    /**
     * Extracts movie ID from Intent used to start this activity.
     */
    private void GetIdFromIntent() {
        Intent intent = getIntent();
        
        if ( intent != null ) {
            m_id = intent.getStringExtra( AdminActivity.KEY_ID );
        }
    }
    
    /**
     * Initialize Views.
     */
    private void InitViews() {
        m_btnReset = findViewById( R.id.button_reset );
        m_btnUpdate = findViewById( R.id.button_update );
        
        m_progressBar = findViewById( R.id.progressBar );
        
        m_includeEdit = findViewById( R.id.include_edit );
        m_reusable = new ReusableEditMovie( m_includeEdit );
        
        m_parent = findViewById( R.id.parent );
    }
    
    /**
     * Initialize http clients for Fmdb api.
     */
    private void InitViewModel() {
        m_fmdbClientApi = ViewModelProviders.of( this ).get( FmdbClientApi.class );
    
        if ( !m_fmdbClientApi.IsFmdbSet() ) {
            m_fmdbClientApi.SetFmdb( new RfFmdb() );
        }
    
        m_fmdbClientApi.GetUpdatedId().observe( this, id -> OnUpdateDone( id ) );
    
        m_fmdbClientApi.GetReadData().observe( this, movieData -> OnResetDone( movieData ) );
    }
    
    /**
     * Set Button click listeners.
     */
    private void InitButtonListeners() {
        m_btnReset.setOnClickListener( view -> OnResetButtonClick() );
        
        m_btnUpdate.setOnClickListener( view -> OnUpdateButtonClick() );
    }
    
    /**
     * Checks if in previous instance state http call was already started or if movie data was
     * received from Firestore.
     */
    private void RestoreInstanceState( Bundle savedInstanceState ) {
        // Check if previous instance state was saved in Bundle.
        if ( savedInstanceState != null ) {
            
            m_wasEdited = savedInstanceState.getBoolean( KEY_EDITED );
            m_isHttpCallStarted = savedInstanceState.getBoolean( KEY_HTTP_CALL_STARTED );
            m_id = savedInstanceState.getString( KEY_ID );
            
            // Check if http call was started in previous instance.
            if ( m_isHttpCallStarted ) {
                
                OnHttpCallStarted();
                
                return;
                
            }
            
        }
        
        // Check if Movie data was cached in Fmdb ViewModel.
        List<MovieData> movieDataList = m_fmdbClientApi.GetCachedData();
        
        if ( movieDataList != null && movieDataList.size() > 0 ) {
    
            MovieData movieData = movieDataList.get( 0 );
            
            m_id = movieData.id;
            m_reusable.SetViews( movieData );
            
            return;
            
        }
        
        // Get Movie data from Firestore.
        m_btnReset.performClick();
    }
    
    /**
     * Handle click on Reset Button. Gets movie data from Firestore.
     */
    private void OnResetButtonClick() {
        m_fmdbClientApi.GetMovieById( m_id );
        
        OnHttpCallStarted();
    }
    
    /**
     * Called when http call to get movie data from Firestore returns.
     */
    private void OnResetDone( List<MovieData> movieDataList ) {
        OnHttpCallDone();
    
        if ( movieDataList != null ) {
            MovieData movieData = movieDataList.get( 0 );
    
            if ( movieData.response ) {
                m_reusable.SetViews( movieData );
                
                return;
            }
        }
    
        Common.MakeSnackbar( this, m_parent, "There was a problem getting movie" );
    }
    
    /**
     * Handle click on Update Button. Takes data from EditText widgets and uploads it to Firebase.
     */
    private void OnUpdateButtonClick() {
        FirebaseAuthIdToken.GetToken( idToken -> {
            if ( idToken != null ) {
    
                OnHttpCallStarted();
    
                MovieData movieData = m_reusable.ToMovieData();
    
                FmdbReqBody fmdbReqBody = new FmdbReqBody();
                fmdbReqBody.token = new FmdbToken( idToken );
                fmdbReqBody.data = movieData.ToFmdbReqData();
    
                m_fmdbClientApi.UpdateMovie( m_id, fmdbReqBody );
                
            } else {
                Common.MakeSnackbar( this, findViewById( R.id.parent ), getString( R.string.text_auth_error ) );
            }
        } );
    }
    
    /**
     * Called when http call to update movie data in Firestore returns.
     * @param id Updated movie ID or -1 if there was error.
     */
    private void OnUpdateDone( String id ) {
        OnHttpCallDone();
        
        if ( id.equals( "-1" ) ) {
            Common.MakeSnackbar( this, m_parent, "There was a problem updating movie" );
        } else {
            
            m_wasEdited = true;
            
            Common.MakeSnackbar( this, m_parent, "Movie updated successfully" );
            
        }
    }
    
    /**
     * Called when http call starts to hide UI.
     */
    private void OnHttpCallStarted() {
        m_isHttpCallStarted = true;
        
        m_btnReset.setEnabled( false );
        m_btnUpdate.setEnabled( false );
        m_progressBar.setVisibility( View.VISIBLE );
        m_includeEdit.setVisibility( View.GONE );
        
        // Hide keyboard.
        Common.HideKeyboard( this );
    }
    
    /**
     * Called when http call returns to show UI.
     */
    private void OnHttpCallDone() {
        m_isHttpCallStarted = false;
        
        m_btnReset.setEnabled( true );
        
        List<MovieData> movieDataList = m_fmdbClientApi.GetCachedData();
        
        if ( movieDataList != null && movieDataList.size() > 0 && movieDataList.get( 0 ).response ) {
            m_btnUpdate.setEnabled( true );
        }
        
        m_progressBar.setVisibility( View.GONE );
        m_includeEdit.setVisibility( View.VISIBLE );
    }
    
}

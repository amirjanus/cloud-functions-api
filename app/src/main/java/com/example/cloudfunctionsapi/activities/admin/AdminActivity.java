package com.example.cloudfunctionsapi.activities.admin;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cloudfunctionsapi.R;
import com.example.cloudfunctionsapi.activities.addItem.AddItemActivity;
import com.example.cloudfunctionsapi.activities.editItem.EditItemActivity;
import com.example.cloudfunctionsapi.common.Common;
import com.example.cloudfunctionsapi.common.FirebaseAuthIdToken;
import com.example.cloudfunctionsapi.common.PageCounter;
import com.example.cloudfunctionsapi.firemovies.FmdbClientApi;
import com.example.cloudfunctionsapi.firemovies.FmdbToken;
import com.example.cloudfunctionsapi.movieData.MovieData;
import com.example.cloudfunctionsapi.retrofit.fmdb.RfFmdb;

import java.util.ArrayList;
import java.util.List;

public class AdminActivity extends AppCompatActivity {
    
    public static final String KEY_ID = "ID";
    private static final String KEY_HTTP_CALL_STARTED = "HTTP_CALL_STARTED";
    
    private static final int REQUEST_CODE_ACTIVITY_EDIT = 100;
    private static final int REQUEST_CODE_ACTIVITY_ADD = 101;
    
    private LinearLayout m_controls;
    private TextView m_textNoItems;
    private TextView m_textCurrentPage;
    private TextView m_textTotalPages;
    private Button m_btnRefresh;
    private Button m_btnAdd;
    private Button m_btnFirstPage;
    private Button m_btnLastPage;
    private Button m_btnNextPage;
    private Button m_btnPreviousPage;
    private ProgressBar m_progressBar;
    private View m_parent;
    
    private FmdbClientApi m_fmdbClientApi;
    
    private MoviesAdminAdapter m_adapter;
    private ArrayList<MovieData> m_dataset;
    
    private PageCounter m_pageCounter;
    
    private boolean m_isHttpCallStarted;
    
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_admin );
        
        m_pageCounter = new PageCounter( savedInstanceState );
        
        InitViews();
        InitRecycler();
        InitViewModel();
        InitButtonListeners();
        
        RestoreInstanceState( savedInstanceState );
    }
    
    @Override
    protected void onSaveInstanceState( Bundle outState ) {
        outState.putBoolean( KEY_HTTP_CALL_STARTED, m_isHttpCallStarted );
        
        m_pageCounter.SaveToBundle( outState );
        
        super.onSaveInstanceState( outState );
    }
    
    @Override
    protected void onActivityResult( int requestCode, int resultCode, @Nullable Intent data ) {
        switch ( requestCode ) {
            case REQUEST_CODE_ACTIVITY_EDIT:
                // We will request new data from server only if item was edited, otherwise we will use cached data from ViewModel.
                if ( resultCode == RESULT_OK ) {
                    m_btnRefresh.performClick();
                }
                
                break;
            
            case REQUEST_CODE_ACTIVITY_ADD:
                // We will request new data from server only if we are last page, otherwise we will use cached data from ViewModel.
                if ( resultCode == RESULT_OK && m_pageCounter.IsAtLastPage() ) {
                    m_btnRefresh.performClick();
                }
                
                break;
        }
    }
    
    /**
     * /**
     * Checks if in previous instance state http call was already started or if movie data was
     * received from Firestore.
     */
    private void RestoreInstanceState( Bundle savedInstanceState ) {
        // Check if http call was started in previous activity instance.
        if ( savedInstanceState != null ) {
            
            m_isHttpCallStarted = savedInstanceState.getBoolean( KEY_HTTP_CALL_STARTED );
            
            if ( m_isHttpCallStarted ) {
                HttpCallStarted();
                
                return;
            }
            
        }
        
        // Check if movie data was cached in ViewModel.
        List<MovieData> movies = m_fmdbClientApi.GetCachedData();
        
        if ( movies != null && movies.size() > 0 ) {
            
            m_dataset.addAll( movies );
            m_adapter.notifyDataSetChanged();
            
            return;
        }
        
        
        // At this point just fetch new data from Firestore.
        m_btnRefresh.performClick();
    }
    
    /**
     * Initialize http client for Fmdb api.
     */
    private void InitViewModel() {
        m_fmdbClientApi = ViewModelProviders.of( this ).get( FmdbClientApi.class );
        
        if ( !m_fmdbClientApi.IsFmdbSet() ) {
            m_fmdbClientApi.SetFmdb( new RfFmdb() );
        }
        
        m_fmdbClientApi.GetReadData().observe( this, data -> HttpCallDone( data ) );
        
        m_fmdbClientApi.GetDeletedId().observe( this, id -> {
            Common.MakeSnackbar( this, m_parent, "Delete movie with ID " + id );
            
            UpdatePageCount();
            
            m_btnRefresh.performClick();
        } );
        
        UpdatePageCount();
    }
    
    /**
     * Initialize RecyclerView to show list of movies.
     */
    private void InitRecycler() {
        m_dataset = new ArrayList<>();
        
        m_adapter = new MoviesAdminAdapter( m_dataset );
        m_adapter.SetOnDeleteClickListener( id -> OnDeleteButtonClick( id ) );
        m_adapter.SetOnEditListener( id -> OnEditButtonClick( id ) );
        
        RecyclerView recyclerView = findViewById( R.id.recycler_admin );
        recyclerView.setLayoutManager( new LinearLayoutManager( this ) );
        recyclerView.setAdapter( m_adapter );
    }
    
    /**
     * Called when Delete Button is clicked. Deletes selected movie in Firestore.
     *
     * @param itemId Selected item ID.
     */
    private void OnDeleteButtonClick( String itemId ) {
        FirebaseAuthIdToken.GetToken( idToken -> {
            if ( idToken != null ) {
    
                FmdbToken token = new FmdbToken( idToken );
    
                m_fmdbClientApi.DeleteMovie( itemId, token );
    
                HttpCallStarted();
                
            } else {
                Common.MakeSnackbar( this, findViewById( R.id.parent ), getString( R.string.text_auth_error ) );
            }
        } );
    }
    
    /**
     * Called when Edit Buttons is clicked. Starts EditItemActivity.
     *
     * @param itemId Selected item ID.
     */
    private void OnEditButtonClick( String itemId ) {
        Intent intent = new Intent( this, EditItemActivity.class );
        intent.putExtra( KEY_ID, itemId );
        
        startActivityForResult( intent, REQUEST_CODE_ACTIVITY_EDIT );
    }
    
    /**
     * Initialize Views.
     */
    private void InitViews() {
        m_controls = findViewById( R.id.layout_controls );
        
        m_textNoItems = findViewById( R.id.text_noItems );
        m_textCurrentPage = findViewById( R.id.text_currentPage );
        m_textTotalPages = findViewById( R.id.text_totalPages );
        
        m_btnRefresh = findViewById( R.id.button_refresh );
        m_btnAdd = findViewById( R.id.button_add );
        m_btnFirstPage = findViewById( R.id.button_firstPage );
        m_btnLastPage = findViewById( R.id.button_lastPage );
        m_btnNextPage = findViewById( R.id.button_nextPage );
        m_btnPreviousPage = findViewById( R.id.button_previousPage );
        
        m_progressBar = findViewById( R.id.progressBar );
        
        m_parent = findViewById( R.id.parent );
    }
    
    /**
     * Set Button click listeners.
     */
    private void InitButtonListeners() {
        // Refresh UI Button.
        m_btnRefresh.setOnClickListener( view -> {
            m_fmdbClientApi.GetMovies( m_pageCounter.m_page, m_pageCounter.m_range );
            HttpCallStarted();
        } );
        
        // Add movie Button.
        m_btnAdd.setOnClickListener( view -> startActivityForResult( new Intent( this, AddItemActivity.class ), REQUEST_CODE_ACTIVITY_ADD ) );
        
        // First page Button.
        m_btnFirstPage.setOnClickListener( view -> {
            if ( !m_pageCounter.IsAtFirstPage() ) {
                
                m_fmdbClientApi.GetMovies( m_pageCounter.SetAndGet( 1 ), m_pageCounter.m_range );
                HttpCallStarted();
                
            }
        } );
        
        // Last page Button.
        m_btnLastPage.setOnClickListener( view -> {
            if ( !m_pageCounter.IsAtLastPage() ) {
                
                m_fmdbClientApi.GetMovies( m_pageCounter.SetAndGet( m_pageCounter.m_totalPages ), m_pageCounter.m_range );
                HttpCallStarted();
                
            }
        } );
        
        // Next page Button.
        m_btnNextPage.setOnClickListener( view -> {
            if ( !m_pageCounter.IsAtLastPage() ) {
                
                m_fmdbClientApi.GetMovies( m_pageCounter.IncrementAndGet(), m_pageCounter.m_range );
                HttpCallStarted();
                
            }
        } );
        
        // Previous page Button.
        m_btnPreviousPage.setOnClickListener( view -> {
            if ( !m_pageCounter.IsAtFirstPage() ) {
                
                m_fmdbClientApi.GetMovies( m_pageCounter.DecrementAndGet(), m_pageCounter.m_range );
                HttpCallStarted();
                
            }
        } );
    }
    
    /**
     * Enables UI buttons.
     **/
    private void EnableButtons() {
        if ( !m_pageCounter.IsAtFirstPage() ) {
            m_btnPreviousPage.setEnabled( true );
            m_btnFirstPage.setEnabled( true );
        }
        
        if ( !m_pageCounter.IsAtLastPage() ) {
            m_btnNextPage.setEnabled( true );
            m_btnLastPage.setEnabled( true );
        }
        
        m_btnRefresh.setEnabled( true );
    }
    
    /**
     * Disables UI buttons.
     **/
    private void DisableButtons() {
        m_btnPreviousPage.setEnabled( false );
        m_btnFirstPage.setEnabled( false );
        m_btnNextPage.setEnabled( false );
        m_btnLastPage.setEnabled( false );
        m_btnRefresh.setEnabled( false );
    }
    
    /**
     * Called when buttons which start http request are clicked.
     */
    private void HttpCallStarted() {
        m_isHttpCallStarted = true;
        
        DisableButtons();
        
        m_progressBar.setVisibility( View.VISIBLE );
        m_textNoItems.setVisibility( View.GONE );
        
        m_dataset.clear();
        m_adapter.notifyDataSetChanged();
    }
    
    /**
     * Called when http response was received.
     *
     * @param movies List of movies.
     */
    private void HttpCallDone( List<MovieData> movies ) {
        m_isHttpCallStarted = false;
        
        m_progressBar.setVisibility( View.GONE );
        
        if ( movies != null ) {
            m_dataset.addAll( movies );
        } else {
            m_dataset = new ArrayList<>( 0 );
        }
    
        m_adapter.notifyDataSetChanged();
        
        // If data base is empty display text message that there are no items and hide controls.
        if ( m_dataset.size() == 0 ) {
            
            m_textNoItems.setVisibility( View.VISIBLE );
            m_controls.setVisibility( View.GONE );
            
        } else {
            
            m_textNoItems.setVisibility( View.GONE );
            m_controls.setVisibility( View.VISIBLE );
            
        }
        
        UpdatePageCount();
        
        EnableButtons();
    }
    
    /**
     * Gets current page from ViewModel and Calculates total number of pages.
     */
    private void UpdatePageCount() {
        m_textCurrentPage.setText( String.valueOf( m_pageCounter.m_page ) );
        m_textTotalPages.setText( String.valueOf( m_pageCounter.CalculateTotalPages( m_fmdbClientApi.GetTotalResults() ) ) );
    }
    
}

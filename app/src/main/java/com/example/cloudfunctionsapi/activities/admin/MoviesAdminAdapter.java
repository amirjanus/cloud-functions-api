package com.example.cloudfunctionsapi.activities.admin;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.cloudfunctionsapi.R;
import com.example.cloudfunctionsapi.movieData.MovieData;

import java.util.ArrayList;

public class MoviesAdminAdapter extends RecyclerView.Adapter<MoviesAdminAdapter.MovieViewHolder> {
    
    public interface OnDeleteClickListener {
        void OnDelete( String itemId );
    }
    
    public interface OnEditClickListener {
        void OnEdit( String itemId );
    }
    
    private OnDeleteClickListener m_clickListener;
    private OnEditClickListener m_editListener;
    
    private ArrayList<MovieData> m_dataset;
    
    public MoviesAdminAdapter( ArrayList<MovieData> dataset ) {
        m_dataset = dataset;
    }
    
    public void SetOnDeleteClickListener( OnDeleteClickListener listener ) {
        m_clickListener = listener;
    }
    
    public void SetOnEditListener( OnEditClickListener listener ) {
        m_editListener = listener;
    }
    
    /**
     * Called when RecyclerView needs a new ViewHolder of the given type to represent
     * an item.
     *
     * @param parent   The ViewGroup into which the new View will be added after it is bound to
     *                 an adapter position.
     * @param viewType The view type of the new View.
     * @return A new ViewHolder that holds a View of the given view type.
     */
    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder( @NonNull ViewGroup parent, int viewType ) {
        View view = LayoutInflater.from( parent.getContext() ).inflate( R.layout.item_movie_admin, parent, false );
        
        return new MovieViewHolder( view );
    }
    
    /**
     * Called by RecyclerView to display the data at the specified position.
     *
     * @param holder   The ViewHolder which should be updated.
     * @param position The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder( @NonNull MovieViewHolder holder, int position ) {
        MovieData current = m_dataset.get( position );
        
        holder.m_textTitle.setText( current.title );
        holder.m_textYear.setText( String.valueOf( current.year ) );
        
        holder.m_btnEdit.setOnClickListener( view -> {
            if ( m_editListener != null ) {
                m_editListener.OnEdit( current.id );
            }
        } );
        
        holder.m_btnDelete.setOnClickListener( view -> {
            if ( m_clickListener != null ) {
                m_clickListener.OnDelete( current.id );
            }
        } );
    }
    
    /**
     * Returns the total number of items in the data set held by the adapter.
     *
     * @return The total number of items in this adapter.
     */
    @Override
    public int getItemCount() {
        return m_dataset.size();
    }
    
    public class MovieViewHolder extends RecyclerView.ViewHolder {
        
        private TextView m_textTitle;
        private TextView m_textYear;
        
        private Button m_btnEdit;
        private Button m_btnDelete;
        
        public MovieViewHolder( @NonNull View itemView ) {
            super( itemView );
            
            m_textTitle = itemView.findViewById( R.id.text_title );
            m_textYear = itemView.findViewById( R.id.text_year );
            
            m_btnEdit = itemView.findViewById( R.id.button_edit );
            m_btnDelete = itemView.findViewById( R.id.button_delete );
        }
    }
}

package com.example.cloudfunctionsapi.activities.client;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.networking.R;
import com.example.networking.activities.displayItem.DisplayItemActivity;
import com.example.networking.firemovies.FmdbClientApi;
import com.example.networking.movieData.MovieData;
import com.example.networking.retrofit.fmdb.RfFmdb;

import java.util.ArrayList;
import java.util.List;

public class ClientActivity extends AppCompatActivity {
    
    private static final String KEY_HTTP_CALL_STARTED = "HTTP_CALL_STARTED";
    private static final String KEY_DATASET_FETCHED = "DATASET_FETCHED";
    
    public static final String EXTRA_ID = "EXTRA_ID";
    
    private FmdbClientApi m_fmdbClientApi;
    
    private TextView m_textNoMovies;
    private ProgressBar m_progressBar;
    
    private MoviesClientAdapter m_adapter;
    private ArrayList<MovieData> m_dataset;
    
    private boolean m_wasHttpCallStarted;
    private boolean m_wasDatasetFetched;
    
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_client );
        
        InitViews();
        InitViewModel();
        
        RestoreLayout( savedInstanceState );
    }
    
    @Override
    protected void onSaveInstanceState( Bundle outState ) {
        outState.putBoolean( KEY_HTTP_CALL_STARTED, m_wasHttpCallStarted );
        
        super.onSaveInstanceState( outState );
    }
    
    /**
     * Initialize Views.
     */
    private void InitViews() {
        m_textNoMovies = findViewById( R.id.text_noMovies );
        
        m_progressBar = findViewById( R.id.progressBar );
        
        m_dataset = new ArrayList<>();
        m_adapter = new MoviesClientAdapter( m_dataset );
        m_adapter.SetOnItemClickListener( id -> OnItemClick( id ) );
        
        RecyclerView recycler = findViewById( R.id.recycler );
        recycler.setLayoutManager( new LinearLayoutManager( this ) );
        recycler.setAdapter( m_adapter );
    }
    
    /**
     * Initialize http client for Fmdb api.
     */
    private void InitViewModel() {
        m_fmdbClientApi = ViewModelProviders.of( this ).get( FmdbClientApi.class );
    
        if ( !m_fmdbClientApi.IsFmdbSet() ) {
            m_fmdbClientApi.SetFmdb( new RfFmdb() );
        }
        
        m_fmdbClientApi.GetReadData().observe( this, movieDataList -> OnGetMoviesDone( movieDataList ) );
    }
    
    /**
     *
     */
    private void RestoreLayout( Bundle savedInstanceState ) {
        // Check if http call to get movies was started in previous instance.
        if ( savedInstanceState != null ) {
            
            m_wasHttpCallStarted = savedInstanceState.getBoolean( KEY_HTTP_CALL_STARTED );
            m_wasDatasetFetched = savedInstanceState.getBoolean( KEY_DATASET_FETCHED );
            
            if ( m_wasHttpCallStarted ) {
                
                HandleUiOnHttpCall();
                
                return;
                
            }
            
        }
        
        // Check if movies were already fetched from Fmdb.
        List<MovieData> movieDataList = m_fmdbClientApi.GetCachedData();
        
        if ( movieDataList != null ) {
            
            if ( movieDataList.size() > 0 ) {
                m_dataset.addAll( movieDataList );
                m_adapter.notifyDataSetChanged();
            } else {
                m_textNoMovies.setVisibility( View.VISIBLE );
            }
            
            return;
            
        }
        
        // Get movies from Fmdb.
        m_fmdbClientApi.GetMovies( 1, 50 );
    }
    
    /**
     * Called when http call to get movies from Fmdb returns. Displays list of movies in RecyclerView
     * or informs user there are no movies.
     *
     * @param fmdbRes List with movie data.
     */
    private void OnGetMoviesDone( List<MovieData> fmdbRes ) {
        m_wasHttpCallStarted = false;
        m_wasDatasetFetched = true;
        
        HandleUiAfterHttpCall();
        
        // Check that fmdbRes were fetched.
        if ( fmdbRes.size() > 0 ) {
            
            m_dataset.addAll( fmdbRes );
            m_adapter.notifyDataSetChanged();
            
        } else {
            
            m_textNoMovies.setVisibility( View.VISIBLE );
            
        }
    }
    
    private void HandleUiOnHttpCall() {
        m_progressBar.setVisibility( View.VISIBLE );
    }
    
    private void HandleUiAfterHttpCall() {
        m_progressBar.setVisibility( View.GONE );
    }
    
    /**
     * Start DisplayItemActivity activity when user click on movie item.
     *
     * @param id Selected item ID.
     */
    private void OnItemClick( String id ) {
        Intent intent = new Intent( this, DisplayItemActivity.class );
        intent.putExtra( EXTRA_ID, id );
        
        startActivity( intent );
    }
}

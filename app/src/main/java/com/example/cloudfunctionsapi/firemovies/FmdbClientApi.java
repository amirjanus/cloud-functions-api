package com.example.cloudfunctionsapi.firemovies;

import android.annotation.SuppressLint;
import android.util.Log;

import com.example.cloudfunctionsapi.movieData.MovieData;
import com.example.cloudfunctionsapi.movieData.MovieDataViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class FmdbClientApi extends MovieDataViewModel {
    
    private static final String TAG = "FmdbClientApi";
    
    private Fmdb m_fmdb;
    
    private int m_totalResults;
    
    public FmdbClientApi() {
        super();
    }
    
    public void SetFmdb( Fmdb fmdb ) {
        m_fmdb = fmdb;
    }
    
    public boolean IsFmdbSet() {
        return m_fmdb != null;
    }
    
    public int GetTotalResults() {
        return m_totalResults;
    }
    
    @SuppressLint( "CheckResult" )
    public void GetMovieById( String id ) {
        m_fmdb.GetMovieById( id ).subscribe(
                fmdbRes -> {
                    ArrayList<MovieData> list = new ArrayList<>( 1 );
                    list.add( new MovieData( fmdbRes ) );
    
                    m_readData.postValue( list );
                },
                throwable -> {
                    m_readData.postValue( null );
            
                    Log.d( TAG, "GetMovieById:Failure", throwable );
                }
        );
    }
    
    @SuppressLint( "CheckResult" )
    public void GetMovies( int page, int range ) {
        m_fmdb.GetMovies( page, range ).subscribe(
                fmdbResList -> {
                    m_totalResults = fmdbResList.totalResults;
    
                    List<MovieData> movieDataList = new ArrayList<>( m_totalResults );
    
                    Observable
                            .fromIterable( fmdbResList.list )
                            .subscribe( fmdbRes -> {
                                MovieData movieData = new MovieData( fmdbRes );
                                movieData.response = fmdbResList.response;
                                movieData.error = movieData.response ? null : fmdbResList.error;
                                
                                movieDataList.add( movieData );
                            } );
    
                    m_readData.postValue( movieDataList );
                },
                throwable -> {
                    m_readData.postValue( null );
            
                    Log.d( TAG, "GetMovies:Failure", throwable );
                }
        );
    }
    
    @SuppressLint( "CheckResult" )
    public void CreateMovie( FmdbReqBody fmdbReqBody ) {
        m_fmdb.CreateMovie( fmdbReqBody ).subscribe(
                movie -> {
                    if ( movie.response ) {
                        m_createdId.postValue( movie.id );
                    } else {
                        m_createdId.postValue( "-1" );
                    }
                },
                throwable -> {
                    m_createdId.postValue( null );
    
                    Log.d( TAG, "CreateMovie:Failure", throwable );
                }
        );
    }
    
    @SuppressLint( "CheckResult" )
    public void DeleteMovie( String id, FmdbToken token ) {
        m_fmdb.DeleteMovie( id, token ).subscribe(
                movie -> {
                    if ( movie.response ) {
                        m_deletedId.postValue( movie.id );
                    } else {
                        m_deletedId.postValue( "-1" );
                    }
                },
                throwable -> {
                    m_deletedId.postValue( null );
    
                    Log.d( TAG, "DeleteMovie:Failure", throwable );
                }
        );
    }
    
    @SuppressLint( "CheckResult" )
    public void UpdateMovie(  String id, FmdbReqBody body ) {
        m_fmdb.UpdateMovie( id, body ).subscribe(
                fmdbRes -> {
                    if ( fmdbRes.response ) {
                        m_updatedId.postValue( fmdbRes.id );
                    } else {
                        m_updatedId.postValue( "-1" );
                    }
                },
                throwable -> {
                    m_updatedId.postValue( null );
    
                    Log.d( TAG, "UpdateMovie:Failure", throwable );
                }
        );
    }
    
}

package com.example.cloudfunctionsapi.firemovies;

public class FmdbReqData {
    
    public String title;
    public int year;
    public String runtime;
    public String genre;
    public String director;
    public String actors;
    public String country;
    public String plot;
    
}

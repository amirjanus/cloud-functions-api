package com.example.cloudfunctionsapi.firemovies;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import com.example.cloudfunctionsapi.movieData.MovieData;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class FmdbResList {
    
    public List<FmdbRes> list = new ArrayList<>( 0 );
    public int totalResults;
    
    public boolean response;
    public String error;
    
    @SuppressLint( "CheckResult" )
    public List<MovieData> ToMovieData() {
        List<MovieData> movieDataList = new ArrayList<>( list.size() );
        
        Observable
                .fromIterable( list )
                .subscribe( movie -> movieDataList.add( movie.ToMovieData() ) );
        
        return movieDataList;
    }
    
    /**
     * Returns a string representation of the object.
     *
     * @return String representation of the object.
     */
    @SuppressLint( "CheckResult" )
    @NonNull
    @Override
    public String toString() {
        if ( response ) {
            StringBuilder builder = new StringBuilder( "OmdbResList\n" );
            
            Observable
                    .fromIterable( list )
                    .subscribe( movie -> builder.append( "Title: " ).append( movie.title ).append( ", Year: " ).append( movie.year ).append( "\n" ) );
            
            return builder.toString();
        } else {
            return "OmdbResList\n" +
                    "Response: " + false + "\n" +
                    "Error: " + error;
        }
    }
}

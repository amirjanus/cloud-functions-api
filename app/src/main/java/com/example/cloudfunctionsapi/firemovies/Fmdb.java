package com.example.cloudfunctionsapi.firemovies;

import io.reactivex.Single;

public interface Fmdb {
    
    String baseUrl = "http://10.0.2.2:5001/";
    
    /**
     * Searches Fmdb for requested movie by movie ID.
     *
     * @param id ID of the movie to fetch.
     */
    Single<FmdbRes> GetMovieById( String id );
    
    /**
     * Returns all movies in Fmdb.
     *
     * @param page  Page number to return.
     * @param range Number of movies per page.
     * @return List of movies.
     */
    Single<FmdbResList> GetMovies( int page, int range );
    
    /**
     * Creates a new movie in Fmdb.
     *
     * @param body FmdbRes data.
     * @return Newly created movie.
     */
    Single<FmdbRes> CreateMovie( FmdbReqBody body );
    
    /**
     * Deletes movie from Fmdb.
     *
     * @param id   ID of movie to delete.
     * @param body User ID token.
     * @return Newly created movie.
     */
    Single<FmdbRes> DeleteMovie( String id, FmdbToken body );
    
    /**
     * Updates movie in Fmdb.
     *
     * @param id   ID of movie to delete.
     * @param body FmdbRes data.
     * @return Updated movie.
     */
    Single<FmdbRes> UpdateMovie( String id, FmdbReqBody body );
    
}

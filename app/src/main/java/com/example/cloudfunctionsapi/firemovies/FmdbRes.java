package com.example.cloudfunctionsapi.firemovies;

import androidx.annotation.NonNull;

import com.example.cloudfunctionsapi.movieData.MovieData;

public class FmdbRes {
    
    public class Body {
        public String title;
        public int year;
        public String runtime;
        public String genre;
        public String director;
        public String actors;
        public String country;
        public String plot;
    }
    
    public String id;
    public String title;
    public int year;
    public String runtime;
    public String genre;
    public String director;
    public String actors;
    public String plot;
    public String country;
    
    public boolean response;
    public String error;
    
    public Body ToBody() {
        Body body = new Body();
        
        body.title = title;
        body.year = year;
        body.runtime = runtime;
        body.genre = genre;
        body.director = director;
        body.actors = actors;
        body.country = country;
        body.plot = plot;
        
        return body;
    }
    
    public MovieData ToMovieData() {
        
        
        return null;
    }
    
    /**
     * Returns a string representation of the object.
     *
     * @return String representation of the object.
     */
    @NonNull
    @Override
    public String toString() {
        if ( response ) {
            return "OmdbRes\n" +
                    "Id: " + id + "\n" +
                    "Title: " + title + "\n" +
                    "Year: " + year;
            
        } else {
            return "OmdbRes\n" +
                    "Response: " + false + "\n" +
                    "Error: " + error;
        }
    }
}

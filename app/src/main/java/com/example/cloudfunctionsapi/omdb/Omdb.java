package com.example.cloudfunctionsapi.omdb;

import io.reactivex.Single;

public interface Omdb {
    
    String baseUrl = "http://www.omdbapi.com/";
    
    /**
     * Searches Omdb for requested movie using movie title.
     *
     * @param title Name of the movie to fetch.
     */
    Single<OmdbRes> GetMovieByTitle( String apiKey, String title );
    
    /**
     * Searches Omdb for requested movie using movie IMDB ID.
     *
     * @param id IMDB ID of the movie to fetch.
     */
    Single<OmdbRes> GetMovieByImdbId( String apiKey, String id );
    
    /**
     * Searches Omdb for requested movie using movie title and year.
     *
     * @param title Name of the movie to fetch.
     * @param year  Year the movie was released.
     */
    Single<OmdbRes> GetMovieByTitleAndYear( String apiKey, String title, int year );
    
    /**
     * Searches Omdb for requested movies using movie title.
     *
     * @param title Name of the movie to fetch.
     * @return List of movies.
     */
    Single<OmdbResList> GetMoviesByTitle( String apiKey, String title );
    
    /**
     * Searches Omdb for requested movies using movie title.
     *
     * @param title Name of the movie to fetch.
     * @param page  Page number to return ( 1 - 100 ).
     * @return List of movies.
     */
    Single<OmdbResList> GetMoviesByTitle( String apiKey, String title, int page );
    
    /**
     * Searches Omdb for requested movies using movie title.
     *
     * @param title Name of the movie to fetch.
     * @param year  Year the movie was released.
     * @return List of movies.
     */
    Single<OmdbResList> GetMoviesByTitleAndYear( String apiKey, String title, int year );
    
    /**
     * Searches Omdb for requested movies using movie title and year of release.
     *
     * @param title Name of the movie to fetch.
     * @param year  Year the movie was released.
     * @param page  Page number to return ( 1 - 100 ).
     * @return List of movies.
     */
    Single<OmdbResList> GetMoviesByTitleAndYear( String apiKey, String title, int year, int page );
}

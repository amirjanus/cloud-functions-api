package com.example.cloudfunctionsapi.omdb;

import android.annotation.SuppressLint;
import android.util.Log;

import com.example.cloudfunctionsapi.movieData.MovieData;
import com.example.cloudfunctionsapi.movieData.MovieDataViewModel;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.functions.Consumer;

public class OmdbClientApi extends MovieDataViewModel {
    
    private static final String TAG = "OmdbClientApi";
    
    private Omdb m_omdb;
    private String m_apiKey;
    
    public OmdbClientApi() {
        super();
    }
    
    public void SetOmdb( Omdb omdb, String apiKey ) {
        m_omdb = omdb;
        m_apiKey = apiKey;
    }
    
    public boolean IsOmdbSet() {
        return m_omdb != null;
    }
    
    private Consumer<OmdbRes> GetMovieSuccess = omdbRes -> {
        List<MovieData> data = new ArrayList<>( 1 );
        data.add( new MovieData( omdbRes ) );
    
        m_readData.postValue( data );
    };
    
    private Consumer<Throwable> GetMovieError = throwable -> {
        m_readData.postValue( null );
        
        Log.d( TAG, "OmdbClientApi:GetMovie:Failure", throwable );
    };
    
    @SuppressLint( "CheckResult" )
    public void GetMovieByTitle( String title ) {
        m_omdb.GetMovieByTitle( m_apiKey, title ).subscribe( GetMovieSuccess, GetMovieError );
    }
    
    @SuppressLint( "CheckResult" )
    public void GetMovieByTitleAndYear( String title, int year ) {
        m_omdb.GetMovieByTitleAndYear( m_apiKey, title, year ).subscribe( GetMovieSuccess, GetMovieError );
    }
    
}

package com.example.cloudfunctionsapi.omdb;

import android.annotation.SuppressLint;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class OmdbResList {
    
    public List<OmdbRes> Search = new ArrayList<>( 0 );
    public int totalResults;
    
    /**
     * Returns a string representation of the object.
     *
     * @return String representation of the object.
     */
    @SuppressLint( "CheckResult" )
    @NonNull
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder( " \n" );
        
        Observable
                .fromIterable( Search )
                .subscribe( movie -> builder.append( "Title: " ).append( movie.Title ).append( ", Year: " ).append( movie.Year ).append( "\n" ) );
        
        return builder.toString();
    }
}

package com.example.cloudfunctionsapi.omdb;

import androidx.annotation.NonNull;

public class OmdbRes {
    
    public String Title;
    public int Year;
    public String Runtime;
    public String Genre;
    public String Director;
    public String Actors;
    public String Country;
    public String Plot;
    
    public boolean Response;
    public String Error;
    
    /**
     * Returns a string representation of the object.
     *
     * @return String representation of the object.
     */
    @NonNull
    @Override
    public String toString() {
        return "OmdbRes\n" +
                "Title: " + Title + "\n" +
                "Year: " + Year;
    }
}

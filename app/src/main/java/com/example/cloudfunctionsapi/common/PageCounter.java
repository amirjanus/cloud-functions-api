package com.example.cloudfunctionsapi.common;

import android.os.Bundle;

import androidx.annotation.NonNull;

public class PageCounter {

    private static final String KEY_PAGE = "PAGE_COUNTER_PAGE";
    private static final String KEY_RANGE = "PAGE_COUNTER_RANGE";
    private static final String KEY_TOTAL_PAGES = "PAGE_COUNTER_TOTAL_PAGES";
    
    public int m_page;
    public int m_range;
    public int m_totalPages;
    
    /**
     * Load PageCounter data from Bundle if it exists, otherwise set it to default values.
     */
    public PageCounter( Bundle savedState ) {
        if ( savedState != null ) {
            RestoreFromBundle( savedState );
        } else {
            SetPageDataToDefault();
        }
    }
    
    /**
     * Load PageCounter data from Bundle.
     */
    private void RestoreFromBundle( @NonNull Bundle savedState ) {
        m_page = savedState.getInt( KEY_PAGE );
        m_range = savedState.getInt( KEY_RANGE );
        m_totalPages = savedState.getInt( KEY_TOTAL_PAGES );
    }
    
    /**
     * Saves PageCounter data to Bundle.
     */
    public void SaveToBundle( Bundle outState ) {
        outState.putInt( KEY_PAGE, m_page );
        outState.putInt( KEY_RANGE, m_range );
        outState.putInt( KEY_TOTAL_PAGES, m_totalPages );
    }
    
    /**
     * Set PageCounter data to their default values.
     */
    private void SetPageDataToDefault() {
        m_page = 1;
        m_range = 5;
        m_totalPages = 1;
    }
    
    public boolean IsAtLastPage() {
        return m_page == m_totalPages;
    }
    
    public boolean IsAtFirstPage() {
        return m_page == 1;
    }
    
    public int IncrementAndGet() {
        ++m_page;
        
        return m_page;
    }
    
    public int DecrementAndGet() {
        --m_page;
        
        return m_page;
    }
    
    public int SetAndGet( int page ) {
        m_page = page;
        
        return page;
    }
    
    public int CalculateTotalPages( int totalResults ) {
        m_totalPages = ( int ) Math.ceil( ( double ) totalResults / m_range );
        
        return m_totalPages;
    }
}

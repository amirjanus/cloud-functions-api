package com.example.cloudfunctionsapi.common;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.example.cloudfunctionsapi.R;
import com.google.android.material.snackbar.Snackbar;

public class Common {
    
    public static void HideKeyboard( Activity activity ) {
        View view = activity.findViewById( android.R.id.content );
        
        if ( view != null ) {
            
            InputMethodManager imm = ( InputMethodManager ) activity.getSystemService( Context.INPUT_METHOD_SERVICE );
            imm.hideSoftInputFromWindow( view.getWindowToken(), 0 );
            
        }
    }
    
    public static void MakeSnackbar( Context context, View view, String message ) {
        Snackbar sb = Snackbar.make( view, message, Snackbar.LENGTH_LONG );
    
        View sbView = sb.getView();
        sbView.setBackgroundColor( context.getResources().getColor( R.color.color_primary_variant ) );
    
        TextView text = sbView.findViewById( com.google.android.material.R.id.snackbar_text );
        text.setTextColor( context.getResources().getColor( R.color.color_on_primary ) );
    
        sb.show();
    }
    
    public static String GetFilePath( Context context, Uri uri ) {
        String[] filePathColumn = { MediaStore.Images.Media.DATA };
        
        Cursor cursor = context.getContentResolver().query( uri, filePathColumn, null, null, null );
    
        if ( cursor == null ) {
            return "";
        }
    
        cursor.moveToFirst();
        
        int columnIndex = cursor.getColumnIndex( filePathColumn[0] );
        
        String path = cursor.getString(columnIndex);
        
        cursor.close();
        
        return path;
    }
    
}

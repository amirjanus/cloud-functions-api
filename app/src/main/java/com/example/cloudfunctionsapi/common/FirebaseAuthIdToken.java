package com.example.cloudfunctionsapi.common;

import android.os.SystemClock;

import com.example.cloudfunctionsapi.common.function.Consumer;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class FirebaseAuthIdToken {
    
    private static String m_idToken;
    private static long m_issuedTimestamp;
    
    public static void GetToken( Consumer<String> consumer ) {
        if ( m_idToken == null || IsTokenInvalid() ) {
            NewIdToken( consumer );
        } else {
            consumer.accept( m_idToken );
        }
    }
    
    private static boolean IsTokenInvalid() {
        long diff = SystemClock.elapsedRealtime() - m_issuedTimestamp;
        
        return diff > 1000 * 30;
    }
    
    private static void NewIdToken( Consumer<String> consumer ) {
        FirebaseAuth auth = FirebaseAuth.getInstance();
        
        FirebaseUser user = auth.getCurrentUser();
        
        if ( user != null ) {
    
            user.getIdToken( true ).addOnCompleteListener( task -> {
                m_idToken = task.isSuccessful() ? task.getResult().getToken() : null;
                m_issuedTimestamp = SystemClock.elapsedRealtime();
    
                consumer.accept( m_idToken );
            } );
            
        } else {
            
            m_idToken = null;
            consumer.accept( null );
            
        }
    }
    
}

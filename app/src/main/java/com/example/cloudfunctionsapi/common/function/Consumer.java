package com.example.cloudfunctionsapi.common.function;

@FunctionalInterface
public interface Consumer<T> {
    void accept( T idToken );
}

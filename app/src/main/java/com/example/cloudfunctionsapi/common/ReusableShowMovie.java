package com.example.cloudfunctionsapi.common;

import android.view.View;
import android.widget.TextView;

import com.example.cloudfunctionsapi.R;
import com.example.cloudfunctionsapi.movieData.MovieData;

public class ReusableShowMovie {
    
    private TextView m_title;
    private TextView m_year;
    private TextView m_runtime;
    private TextView m_genre;
    private TextView m_directors;
    private TextView m_actors;
    private TextView m_country;
    private TextView m_plot;
    
    /**
     * Initializes this class.
     *
     * @param reusableView View in which reusable layout is included
     */
    public ReusableShowMovie( View reusableView ) {
        InitViews( reusableView );
    }
    
    private void InitViews( View view ) {
        m_title = view.findViewById( R.id.text_title );
        m_year = view.findViewById( R.id.text_year );
        m_runtime = view.findViewById( R.id.text_runtime );
        m_genre = view.findViewById( R.id.text_genre );
        m_directors = view.findViewById( R.id.text_directors );
        m_actors = view.findViewById( R.id.text_actors );
        m_country = view.findViewById( R.id.text_country );
        m_plot = view.findViewById( R.id.text_plot );
    }
    
    public void SetViews( MovieData movie ) {
        m_title.setText( movie.title );
        m_year.setText( String.valueOf( movie.year ) );
        m_runtime.setText( movie.runtime );
        m_genre.setText( movie.genre );
        m_directors.setText( movie.director );
        m_actors.setText( movie.actors );
        m_country.setText( movie.country );
        m_plot.setText( movie.plot );
    }
    
}

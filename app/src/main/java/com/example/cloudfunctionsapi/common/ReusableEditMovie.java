
package com.example.cloudfunctionsapi.common;

import android.view.View;
import android.widget.EditText;

import com.example.cloudfunctionsapi.R;
import com.example.cloudfunctionsapi.movieData.MovieData;

public class ReusableEditMovie {
    
    private EditText m_title;
    private EditText m_year;
    private EditText m_runtime;
    private EditText m_genre;
    private EditText m_directors;
    private EditText m_actors;
    private EditText m_country;
    private EditText m_plot;
    
    public ReusableEditMovie( View reusableView ) {
        InitViews( reusableView );
    }
    
    private void InitViews( View view ) {
        m_title = view.findViewById( R.id.editText_title );
        m_year = view.findViewById( R.id.editText_year );
        m_runtime = view.findViewById( R.id.editText_runtime );
        m_genre = view.findViewById( R.id.editText_genre );
        m_directors = view.findViewById( R.id.editText_directors );
        m_actors = view.findViewById( R.id.editText_actors );
        m_country = view.findViewById( R.id.editText_country );
        m_plot = view.findViewById( R.id.editText_plot );
    }
    
    public void SetViews( MovieData movieData ) {
        m_title.setText( movieData.title );
        m_year.setText( String.valueOf( movieData.year ) );
        m_runtime.setText( movieData.runtime );
        m_genre.setText( movieData.genre );
        m_directors.setText( movieData.director );
        m_actors.setText( movieData.actors );
        m_country.setText( movieData.country );
        m_plot.setText( movieData.plot );
    }
    
    public MovieData ToMovieData() {
        MovieData movieData = new MovieData();
    
        movieData.title = m_title.getText().toString();
        movieData.year = Integer.parseInt( m_year.getText().toString() );
        movieData.runtime = m_runtime.getText().toString();
        movieData.genre = m_genre.getText().toString();
        movieData.director = m_directors.getText().toString();
        movieData.actors = m_actors.getText().toString();
        movieData.country = m_country.getText().toString();
        movieData.plot = m_plot.getText().toString();
    
        return movieData;
    }
    
}

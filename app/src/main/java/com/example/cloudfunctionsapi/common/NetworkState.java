package com.example.cloudfunctionsapi.common;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;

public class NetworkState {
    
    /**
     * Implement this interface to get notified about network changes.
     */
    public interface OnChange {
        void OnConnected();
        void OnDisconnected();
    }
    
    // Reference to class that implement OnChange interface.
    private OnChange m_listener;
    
    // Class that answers queries about the state of network connectivity.
    private ConnectivityManager m_connectivityManager;
    
    // Callbacks for receiving networks changes ( API >= 26 ).
    private NetworkCallback m_networkCallback;
    private NetworkCallbackTimeout m_networkCallbackTimeout;
    
    // Receiver for receiving networks changes ( API < 26 ).
    private BroadcastReceiver m_receiver;
    
    // App context.
    private Context m_context;
    
    public NetworkState( Context context ) {
        m_context = context;
        
        // ConnectivityManager answers queries about the state of network connectivity and notifies applications when network connectivity changes.
        m_connectivityManager = ( ConnectivityManager ) m_context.getSystemService( Context.CONNECTIVITY_SERVICE );
    }
    
    /**
     * Register OnChange interface to receive notifications about network changes.
     *
     * @param listener Class that implements OnChange interface.
     */
    public void RegisterCallback( OnChange listener ) {
        if ( m_listener == null ) {
            
            m_listener = listener;
            
            if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.O ) {
                RequestNetwork();
            } else {
                CheckInternetConnection();
            }
        }
    }
    
    /**
     * Unregister OnChange interface to stop receiving notifications about network changes.
     */
    public void UnregisterCallback() {
        if ( m_listener != null ) {
            
            if ( m_networkCallback != null ) {
                
                m_connectivityManager.unregisterNetworkCallback( m_networkCallback );
                m_networkCallback = null;
                
            } else if ( m_networkCallbackTimeout != null ) {
                
                m_connectivityManager.unregisterNetworkCallback( m_networkCallbackTimeout );
                m_networkCallbackTimeout = null;
                
            } else if ( m_receiver != null ) {
                
                m_context.unregisterReceiver( m_receiver );
                m_receiver = null;
                
            }
            
            m_listener = null;
            
        }
    }
    
    /**
     * Creates BroadcastReceiver to receive notifications about network changes.
     */
    private void CheckInternetConnection() {
        m_receiver = new BroadcastReceiver() {
            @Override
            public void onReceive( Context context, Intent intent ) {
                Bundle bundle = intent.getExtras();
                
                if ( bundle == null ) {
                    return;
                }
                
                if ( bundle.getBoolean( ConnectivityManager.EXTRA_NO_CONNECTIVITY ) ||
                        bundle.getBoolean( ConnectivityManager.EXTRA_IS_FAILOVER ) ||
                        bundle.getBoolean( ConnectivityManager.EXTRA_REASON ) ) {
                    m_listener.OnDisconnected();
                } else {
                    GetInternetStatus();
                }
            }
        };
        
        IntentFilter intentFilter = new IntentFilter( ConnectivityManager.CONNECTIVITY_ACTION );
        
        m_context.registerReceiver( m_receiver, intentFilter );
    }
    
    /**
     * Checks if we are connected to internet using Wifi network. This one time event.
     */
    private void GetInternetStatus() {
        // Get details about the currently active default data network.
        final NetworkInfo networkInfo = m_connectivityManager.getActiveNetworkInfo();
        
        if ( networkInfo != null && networkInfo.isConnected() ) {
            
            // Check if this network is Wifi network.
            if ( networkInfo.getType() == ConnectivityManager.TYPE_WIFI ) {
                m_listener.OnConnected();
            }
            
        }
    }
    
    /**
     * Registers ConnectivityManager network callback.
     */
    private void RegisterNetworkCallback() {
        NetworkRequest request = CreateNetworkRequest();
        
        // Callback that the system will call as networks change state.
        m_networkCallback = new NetworkCallback();
        
        // Register network callback to receive notifications about all networks which satisfy our NetworkRequest.
        m_connectivityManager.registerNetworkCallback( request, m_networkCallback );
    }
    
    /**
     * Registers ConnectivityManager network callback with a timeout of 1 sec. We are using function
     * with timeout so that ConnectivityManager.NetworkCallback:onUnavailable
     */
    @RequiresApi( api = Build.VERSION_CODES.O )
    private void RequestNetwork() {
        NetworkRequest request = CreateNetworkRequest();
        
        // Callback that the system will call as networks change state.
        m_networkCallbackTimeout = new NetworkCallbackTimeout();
        
        // Request a network to satisfy a set of NetworkCapabilities, limited by a timeout.
        m_connectivityManager.requestNetwork( request, m_networkCallbackTimeout, 1000 );
    }
    
    /**
     * Creates NetworkRequest object for use in network callback.
     */
    private NetworkRequest CreateNetworkRequest() {
        // Builder used to create NetworkRequest object.
        NetworkRequest.Builder builder = new NetworkRequest.Builder();
        
        // We want network that uses a Wi-Fi transport.
        builder.addTransportType( NetworkCapabilities.TRANSPORT_WIFI );
        
        // We want network able to reach the internet.
        builder.addCapability( NetworkCapabilities.NET_CAPABILITY_INTERNET );
        
        // Build NetworkRequest from the given set of capabilities.
        return builder.build();
    }
    
    /**
     * Base class for NetworkRequest callbacks. Used for notifications about network changes. This
     * class is for ConnectivityManager:requestNetwork().
     */
    private class NetworkCallbackTimeout extends ConnectivityManager.NetworkCallback {
        
        /**
         * Called when the framework connects and has declared a new network ready for use.
         */
        @Override
        public void onAvailable( Network network ) {
            m_connectivityManager.unregisterNetworkCallback( m_networkCallbackTimeout );
            m_networkCallbackTimeout = null;
            
            RegisterNetworkCallback();
        }
        
        /**
         * Called if no network is found in the timeout time specified in
         * ConnectivityManager:requestNetwork() function.
         */
        @Override
        public void onUnavailable() {
            m_connectivityManager.unregisterNetworkCallback( m_networkCallbackTimeout );
            m_networkCallbackTimeout = null;
            
            if ( m_listener != null ) {
                
                m_listener.OnDisconnected();
                
                RegisterNetworkCallback();
                
            }
        }
    }
    
    /**
     * Base class for NetworkRequest callbacks. Used for notifications about network changes. This
     * class is for ConnectivityManager:registerNetworkCallback().
     */
    private class NetworkCallback extends ConnectivityManager.NetworkCallback {
        
        /**
         * Called when the framework connects and has declared a new network ready for use.
         */
        @Override
        public void onAvailable( Network network ) {
            if ( m_listener != null ) {
                m_listener.OnConnected();
            }
        }
        
        /**
         * Called when the framework has a hard loss of the network or when the graceful failure ends.
         */
        @Override
        public void onLost( Network network ) {
            if ( m_listener != null ) {
                m_listener.OnDisconnected();
            }
        }
        
        /**
         * Called when the network the framework connected to for this request changes capabilities
         * but still satisfies the stated need.
         */
        @Override
        public void onCapabilitiesChanged( Network network, NetworkCapabilities networkCapabilities ) {}
        
    }
    
}

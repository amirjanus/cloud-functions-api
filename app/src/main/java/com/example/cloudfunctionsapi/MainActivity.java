package com.example.cloudfunctionsapi;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.networking.activities.admin.AdminActivity;
import com.example.networking.activities.client.ClientActivity;
import com.example.networking.common.Common;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    
    private FirebaseAuth m_auth;
    
    private Button m_btnAdmin;
    private Button m_btnClient;
    private Button m_btnLogin;
    private View m_parent;
    
    private String m_email;
    private String m_password;
    
    @SuppressLint( "CheckResult" )
    @Override
    protected void onCreate( Bundle savedInstanceState ) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main );
        
        InitAuth();
        
        InitViews();
        InitButtonListeners();
    }
    
    /**
     * Initialize Firebase Authentication and get login credentials.
     */
    private void InitAuth() {
        m_auth = FirebaseAuth.getInstance();
        
        m_email = getString( R.string.email );
        m_password = getString( R.string.password );
    }
    
    /**
     * Initialize Views.
     */
    private void InitViews() {
        m_btnAdmin = findViewById( R.id.button_admin );
        m_btnClient = findViewById( R.id.button_client );
        m_btnLogin = findViewById( R.id.button_login );
        
        m_parent = findViewById( R.id.parent );
        
        // If user is currently signed in, set Login Button text "Sign Out".
        if ( IsUserSignedIn() ) {
            m_btnLogin.setText( getString( R.string.label_sign_out ) );
        }
    }
    
    /**
     * Set Button click listeners.
     */
    private void InitButtonListeners() {
        m_btnAdmin.setOnClickListener( view -> OnButtonAdminClick() );
        
        m_btnClient.setOnClickListener( view -> startActivity( new Intent( this, ClientActivity.class ) ) );
        
        m_btnLogin.setOnClickListener( view -> OnButtonLoginClick() );
    }
    
    /**
     * Handle user clicking SignIn Button. Sign in user to Firebase if not signed already, otherwise
     * sign out.
     */
    private void OnButtonLoginClick() {
        if ( IsUserSignedIn() ) {
            
            m_auth.signOut();
            
            m_btnLogin.setText( R.string.label_sign_in );
            
        } else {
            
            m_btnLogin.setEnabled( false );
            
            m_auth.signInWithEmailAndPassword( m_email, m_password ).addOnCompleteListener( task -> {
                if ( task.isSuccessful() ) {
                    
                    Common.MakeSnackbar( this, m_parent, "Successfully signed in" );
    
                    m_btnLogin.setText( R.string.label_sign_out );
                    
                } else {
                    
                    Common.MakeSnackbar( this, m_parent, "There was a problem signing in" );
                    
                    Log.d( "MainActivity", "signInWithEmail:failure", task.getException() );
                    
                }
                
                m_btnLogin.setEnabled( true );
            } );
            
        }
    }
    
    /**
     * Handle user clicking Admin Button. User can proceed to Admin screen only when signed in.
     */
    private void OnButtonAdminClick() {
        if ( IsUserSignedIn() ) {
            startActivity( new Intent( this, AdminActivity.class ) );
        } else {
            Common.MakeSnackbar( this, m_parent, "Sign in to access admin area" );
        }
    }
    
    /**
     * Check if user is signed in to Firebase.
     *
     * @return True if user is signed, false otherwise.
     */
    private boolean IsUserSignedIn() {
        FirebaseUser user = m_auth.getCurrentUser();
        
        return user != null;
    }

}

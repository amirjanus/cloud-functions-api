package com.example.cloudfunctionsapi.retrofit.omdb;

import com.example.cloudfunctionsapi.omdb.Omdb;
import com.example.cloudfunctionsapi.omdb.OmdbRes;
import com.example.cloudfunctionsapi.omdb.OmdbResList;
import com.example.cloudfunctionsapi.retrofit.RetrofitBase;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class RfOmdb extends RetrofitBase implements Omdb {
    
    private RfOmdbService m_service;
    
    public RfOmdb() {
        super( Omdb.baseUrl );
    }
    
    @Override
    protected void CreateService( Retrofit retrofit ) {
        m_service = retrofit.create( RfOmdbService.class );
    }
    
    /**
     * Searches Omdb for requested movie using movie title.
     *
     * @param title Name of the movie to fetch.
     */
    @Override
    public Single<OmdbRes> GetMovieByTitle( String apiKey, String title ) {
        return Single
                .fromCallable( () -> m_service.GetMovieByTitle( apiKey, title ).execute() )
                .subscribeOn( Schedulers.io() )
                .map( response -> response.body() );
    }
    
    /**
     * Searches Omdb for requested movie using movie IMDB ID.
     *
     * @param id IMDB ID of the movie to fetch.
     */
    @Override
    public Single<OmdbRes> GetMovieByImdbId( String apiKey, String id ) {
        return Single
                .fromCallable( () -> m_service.GetMovieByImdbId( apiKey, id ).execute() )
                .subscribeOn( Schedulers.io() )
                .map( response -> response.body() );
    }
    
    /**
     * Searches Omdb for requested movie using movie title and year.
     *
     * @param title Name of the movie to fetch.
     * @param year  Year the movie was released.
     */
    @Override
    public Single<OmdbRes> GetMovieByTitleAndYear( String apiKey, String title, int year ) {
        return Single
                .fromCallable( () -> m_service.GetMovieByTitleAndYear( apiKey, title, year ).execute() )
                .subscribeOn( Schedulers.io() )
                .map( response -> response.body() );
    }
    
    /**
     * Searches Omdb for requested movies using movie title.
     *
     * @param title Name of the movie to fetch.
     * @return List of movies.
     */
    @Override
    public Single<OmdbResList> GetMoviesByTitle( String apiKey, String title ) {
        return GetMoviesByTitle( apiKey, title, 1 );
    }
    
    /**
     * Searches Omdb for requested movies using movie title.
     *
     * @param title Name of the movie to fetch.
     * @param page  Page number to return ( 1 - 100 ).
     * @return List of movies.
     */
    @Override
    public Single<OmdbResList> GetMoviesByTitle( String apiKey, String title, int page ) {
        return Single
                .fromCallable( () -> m_service.GetMoviesByTitle( apiKey, title, page ).execute() )
                .subscribeOn( Schedulers.io() )
                .map( response -> response.body() );
    }
    
    /**
     * Searches Omdb for requested movies using movie title.
     *
     * @param title Name of the movie to fetch.
     * @param year  Year the movie was released.
     * @return List of movies.
     */
    @Override
    public Single<OmdbResList> GetMoviesByTitleAndYear( String apiKey, String title, int year ) {
        return GetMoviesByTitleAndYear( apiKey, title, year, 1 );
    }
    
    /**
     * Searches Omdb for requested movies using movie title and year of release.
     *
     * @param title Name of the movie to fetch.
     * @param year  Year the movie was released.
     * @param page  Page number to return ( 1 - 100 ).
     * @return List of movies.
     */
    @Override
    public Single<OmdbResList> GetMoviesByTitleAndYear( String apiKey, String title, int year, int page ) {
        return Single
                .fromCallable( () -> m_service.GetMoviesByTitleAndYear( apiKey, title, year, page ).execute() )
                .subscribeOn( Schedulers.io() )
                .map( response -> response.body() );
    }
}

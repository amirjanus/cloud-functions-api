package com.example.cloudfunctionsapi.retrofit.fmdb;

import com.example.cloudfunctionsapi.firemovies.FmdbReqBody;
import com.example.cloudfunctionsapi.firemovies.FmdbRes;
import com.example.cloudfunctionsapi.firemovies.FmdbResList;
import com.example.cloudfunctionsapi.firemovies.FmdbToken;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface RfFmdbService {
    
    String path = "/my-functions-80360/us-central1/movies/";
    
    @GET( path )
    Call<FmdbRes> GetMovieById(
            @Query( "id" ) String id
    );
    
    @GET( path )
    Call<FmdbResList> GetMovies(
            @Query( "page" ) int page,
            @Query( "range" ) int range
    );
    
    @POST( path )
    Call<FmdbRes> CreateMovie(
            @Body() FmdbReqBody body
    );
    
    //@DELETE( path )
    @HTTP( method = "DELETE", path = path, hasBody = true )
    Call<FmdbRes> DeleteMovie(
            @Query( "id" ) String id,
            @Body() FmdbToken body
    );
    
    @PUT( path )
    Call<FmdbRes> UpdateMovie(
            @Query( "id" ) String id,
            @Body() FmdbReqBody body
    );
    
    @Multipart
    @POST( path + "poster/" )
    Call<FmdbRes> UploadPoster(
            @Part MultipartBody.Part file,
            @Part( "name" ) RequestBody requestBody
    );
    
}

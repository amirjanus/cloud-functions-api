package com.example.cloudfunctionsapi.retrofit.fmdb;

import com.example.cloudfunctionsapi.firemovies.Fmdb;
import com.example.cloudfunctionsapi.firemovies.FmdbReqBody;
import com.example.cloudfunctionsapi.firemovies.FmdbRes;
import com.example.cloudfunctionsapi.firemovies.FmdbResList;
import com.example.cloudfunctionsapi.firemovies.FmdbToken;
import com.example.cloudfunctionsapi.retrofit.RetrofitBase;

import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class RfFmdb extends RetrofitBase implements Fmdb {
    
    private RfFmdbService m_service;
    
    public RfFmdb() {
        super( Fmdb.baseUrl );
    }
    
    @Override
    protected void CreateService( Retrofit retrofit ) {
        m_service = retrofit.create( RfFmdbService.class );
    }
    
    /**
     * Searches Fmdb for requested movie by movie ID.
     *
     * @param id ID of the movie to fetch.
     */
    @Override
    public Single<FmdbRes> GetMovieById( String id ) {
        return Single
                .fromCallable( () -> m_service.GetMovieById( id ).execute() )
                .subscribeOn( Schedulers.io() )
                .map( response -> response.body() );
    }
    
    /**
     * Returns all movies in Fmdb.
     *
     * @param page  Page number to return.
     * @param range Number of movies per page.
     * @return List of movies.
     */
    @Override
    public Single<FmdbResList> GetMovies( int page, int range ) {
        return Single
                .fromCallable( () -> m_service.GetMovies( page, range ).execute() )
                .subscribeOn( Schedulers.io() )
                .map( response -> response.body() );
    }
    
    /**
     * Creates a new movie in Fmdb.
     *
     * @param fmdbReqBody FmdbRes data.
     * @return Newly created movie.
     */
    @Override
    public Single<FmdbRes> CreateMovie( FmdbReqBody fmdbReqBody ) {
        return Single
                .fromCallable( () -> m_service.CreateMovie( fmdbReqBody ).execute() )
                .subscribeOn( Schedulers.io() )
                .map( response -> response.body() );
    }
    
    /**
     * Deletes movie from Fmdb.
     *
     * @param id        ID of movie to delete.
     * @param fmdbToken User ID token.
     * @return Newly created movie.
     */
    @Override
    public Single<FmdbRes> DeleteMovie( String id, FmdbToken fmdbToken ) {
        return Single
                .fromCallable( () -> m_service.DeleteMovie( id, fmdbToken ).execute() )
                .subscribeOn( Schedulers.io() )
                .map( response -> response.body() );
    }
    
    /**
     * Updates movie in Fmdb.
     *
     * @param id          ID of movie to delete.
     * @param fmdbReqBody FmdbRes data.
     * @return Updated movie.
     */
    @Override
    public Single<FmdbRes> UpdateMovie( String id, FmdbReqBody fmdbReqBody ) {
        return Single
                .fromCallable( () -> m_service.UpdateMovie( id, fmdbReqBody ).execute() )
                .subscribeOn( Schedulers.io() )
                .map( response -> response.body() );
    }
    
}

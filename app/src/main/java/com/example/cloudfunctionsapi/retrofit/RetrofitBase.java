package com.example.cloudfunctionsapi.retrofit;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public abstract class RetrofitBase {
    
    /**
     * Initializes Omdb service.
     */
    public RetrofitBase( String baseUrl ) {
        
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor( message -> Log.d( "OkHttp", message ) );
        logging.setLevel( HttpLoggingInterceptor.Level.BASIC );
        
        // Build Retrofit object.
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.readTimeout( 30, TimeUnit.SECONDS );
        httpClient.writeTimeout( 30, TimeUnit.SECONDS );
        httpClient.connectTimeout( 30, TimeUnit.SECONDS );
        
        httpClient.addInterceptor( logging );
        
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl( baseUrl )
                .addConverterFactory( GsonConverterFactory.create() )
                .client( httpClient.build() )
                .build();
        
        // Create RfOmdbService object.
        CreateService( retrofit );
    }
    
    /**
     * Implement this method to create service from http call interface. Inside this function just
     * write the following statement:
     * YourService service = retrofit.create( YourService.class );
     */
    protected abstract void CreateService( Retrofit retrofit );
    
}

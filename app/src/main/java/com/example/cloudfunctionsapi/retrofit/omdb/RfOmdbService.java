package com.example.cloudfunctionsapi.retrofit.omdb;

import com.example.cloudfunctionsapi.omdb.OmdbRes;
import com.example.cloudfunctionsapi.omdb.OmdbResList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RfOmdbService {
    
    @GET( "/" )
    Call<OmdbRes> GetMovieByTitle(
            @Query( "apikey" ) String apiKey,
            @Query( "t" ) String title
    );
    
    @GET( "/" )
    Call<OmdbRes> GetMovieByTitleAndYear(
            @Query( "apikey" ) String apiKey,
            @Query( "t" ) String title,
            @Query( "y" ) int year
    );
    
    @GET( "/" )
    Call<OmdbRes> GetMovieByImdbId(
            @Query( "apikey" ) String apiKey,
            @Query( "i" ) String id
    );
    
    @GET( "/" )
    Call<OmdbResList> GetMoviesByTitle(
            @Query( "apikey" ) String apiKey,
            @Query( "s" ) String title,
            @Query( "page" ) int page
    );
    
    @GET( "/" )
    Call<OmdbResList> GetMoviesByTitleAndYear(
            @Query( "apikey" ) String apiKey,
            @Query( "s" ) String title,
            @Query( "y" ) int year,
            @Query( "page" ) int page
    );
    
}
